10/26/2022
- We are deployed and functional! (Mostly)
- Dylan MVPed the final deployment, it was a group effort with him screen sharing.
- Today was a bug fix of blacklist/favorites adding from the front end, the behavior was unexpected post deployment
- May need to fix some typos in readme, but MVP is done!

10/25/2022
- Pair programming as a full group today, as deployment is our final step.
    - Worked on cleaning piplines and updating ci.yml file for deployment.
    - Tried to attach database to heroku project. Currently a WIP
    - Added many variables to gitlabs CI/CD section.

10/24/2022
- Worked with Mimi on frontend for blacklist and favorites. Renamed blacklist to dislikes.
- Connected with Dylan about his fix to the backend.
- Watched Mimi try to fix the refresh issue on our Userpage.
- Pair programmed with Lee to fix the table design on the userpage

10/23/2022
- Worked with Dylan and Mimi on the backend issue again.
- Worked on a unit test with Dylan, did not finish.
- The infinite loop from yesterday is still happening.
- Determined we may need help from instructors.

10/22/2022
- Worked with Dylan and Mimi on backend issues involving the blacklist / favorites. 
- Did not come to a conclusion, the backend is just not returning the data we wanted.
- Mimi and I caused an infinite loop on the frontend involving a UseEffect function.

10/20/2022
- Resolved some conflicts with fellow group members
- Looked into issues on gitlab, alerted Eric to check them out
- Tested site functionality by poking everything I can poke.
- Moved a bunch of files into folders, cleaned up file tree. Reminder: DELETE SAMPLE_SERVICE folder!!

10/19/2022
- Wrote unit test for duplicate account. Two unit tests in!
- Connected with Dylan about a second database
- Fixed Profile button to no longer have a dropdown
- Fixed some files based off flake8 recommendations

10/18/2022
- Written unit test for create account. May write more later to practice.
- Pair programmed with Dylan / Mimi on stretch goal of blacklist / favorites list. 

10/17/2022
- Worked on accessing account details from front-end.
    - Required making an account variable in auth.js
- Wrote a unit test for a failed return from the yelp_api. Was going to write one for a valid return but Eric beat me to it.

10/14/2022
- Worked on adding some more data to the restaurant display.
- May work on connecting some functionality to the user favorites / blacklist.
- Realized we needed to connect backend to frontend for accounts
- Cleaned up some code and fixed some fonts. Added Yelp logo also.

10/13/2022
- Made a Notes branch for all team members to put commented code in.
- Refactoring code and renaming some parts to be more clear as to what they do.

10/12/2022
- Cleaning up finished code in yelp_api.py, rollform.js, and result.js
- Moving on to front-end later today.
- Organizing the file trees
- Massive merge with main with all the branches of group members

10/11/2022
- Figuring out how to pass data between functional components.
    - With help from SEIRs, able to pass data between Rollform.js and Result.js, restaurant info is showing up!
    - Next step is organizing all of it into data on the webpage that isn't a jumbled mess.

10/10/2022
- MongoDB is up and running. Had to ask for assistance to double-check, but it works! Need to access the DB from the Windows Powershell instead of Docker's terminal.
- Currently working on RollForm and maybe the Results page for front-end.

10/8/2022
- Worked on Rollform.js a bit, have a form showing up at localhost:3000
- Added a Nav.js and Footer.js ... neither page is working currently
- Experimented with App.js putting all the pages together.

10/6/2022
- Pair programmed with Mimi to address an error with the Yelp API call. 
    - If the API response was an empty dictionary, we would receieve and error. We managed to fix this with a try/except block. If the dictionary was empty, it would return a response of "Invalid parameters" or something along those lines. 

10/5/2022
- Pair programmed with Mimi to create the Yelp API call using FastAPI.
    - Ran into an edge case where people could search for things other than restaurants with some search terms.
- Demonstrated Yelp API calls using Insomnia to whole group.
- With the collaboration we were able to figure out how to reduce the amount of non-food businesses the Yelp API puts out when a search is executed.

10/4/2022
- Ran docker-compose build -> docker-compose up -> Runs as expected. 
    Ran into an issue with the ghi container, was fixed with input from Dylan to change the file from CRLF to LF.
- Created first journal entry