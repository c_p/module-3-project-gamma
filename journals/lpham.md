## Lee Pham Journal
* Date: 10/04/2022
  * A list of features/issues that you worked on and who you worked with, if applicable
    * Started creating models with attributes for later use in the SPA
      * User Profiles: User, Restaurant, Temporary(?)
    * Worked together with group to fix bugs on our individual machines to successfully run the docker containers
    * Updated run.sh file to be LF instead of CRLF
    * Created Yelp API Key and account
    * Verified that our heroku app is currently deployed and looking like it works
    * Coded alongside the configuration of our gitlab-ci file to understand how we reference each part
    * Instructions for deploying the project template:
  ```
          1) Create a heroku account and app
          2) edit `.gitlab-ci.yml`
              1) PUBLIC_URL : this is the gitlab pages URL
              2) REACT_APP_API_HOST : this is the URL to the service on Heroku
          3) Heroku Variables:
              1) CORS_HOST
          4) GitLab Variables:
              - `HEROKU_API_KEY` : (protected and masked)
              - `HEROKU_FASTAPI_APP`: name of the service in heroku
              - `REACT_APP_API_HOST`: this is the URL to the service on Heroku
          5) Push to main
  ```
  * A reflection on any design conversations that you had
    * Spoke on scalability issue for fetch requests from the Yelp API (limit to 5000 requests)
    * Set to look for fetch requests for Google Maps API and how we will set temporary locations per session
  * At least one ah-ha! moment that you had during your coding, however small
    * When debugging, if you have some very basic issues. no matter how complex it looks, just feel free to restart your computer or the program. (sometimes even after deleting a dependency folder which might have been corrupted)


* Date: 10/05/2022
  * Started listing out what I need to do to getting the geo-location api and how to configure it to our website.
  * Looked up how to handle temporary sessions for guest users
  * Spoke to team to get on the same page regarding how our favorites and black lists within each user profile will be used
    * blacklist is an additional filter constraint
    * favorites is a list to iterate over randomly for a favorite random result

  * Date: 10/06/2022
  * Went over authentication : FastAPI, React, Micro-services
    * spoke about security tokens for login


* Date: 10/10/2022
  * Created Diagram for how to implement a CRUD app using FastAPI and React
  * Downloaded Beekeeper, needing to look up a tutorial on how to use it
  * Spoke in standup regarding how we will implement our tables via MongoDB
    * Discussed React Hooks so that we can utilize them throughout our build
      * I still need to look more into them later on because I'm not sure how we're going to implement it fully
      * Talk about me doing the login/filters modals, (will need to request how I should set up my arguments for the filter modal)
      * MongoDB has been giving us authentication errors (we need mongoexpress vs mongo, similar to pgadmin to psql)
      * Carmen Might have ran the database but isn't sure if she had set up the database fully
      * Our current Blocker is mongo authentication, so our goal is to overcome this and see how far we can get after
  * Starting on Nav.JS so I can feel like I'm doing something, I've been pretty loss this whole time. 
  * I'll get to the JS file for my home page after

* Date: 10/11/2022
  * Went over React hooks and Redux toolkit
    * waiting to still see how Dylan is getting his user model to work for login/authorization
  * able to overcome previous blocker with mongo authentication and getting into mongosh
  * agreed that we want to finish the project before deployment by this Friday
  * I'll be working on the footer js file
  * Mimi and Carmen are going to work on getting the restaurants to display today


* Date: 10/12/2022
  * Went over websockets and if we'll be implementing them~ nope
  * Finished making the Nav, Footer, and MainPage files yesterday, mainpage is mostly a template placeholder though
  * Waiting to see how we can mix both mimi/carmens side of things with mine on the main page
  * agreed that we want to finish the project before deployment by this Friday
  * Mimi and Carmen are going to work on getting the restaurants to display today
  * I need to look up how we are going to apply filters to the list, So I think I need to wait for Mimi/Carmen for that
    * In the mean time, I'll just work on trying to create a modal and place it in our page somewhere until we can reference it
    * Worked with dylan as driver to create our login modal
    * was able to successfully get modal to show up in the middle and on the top layer of page
    * worked together to figure out how to supply login information using usestate and useeffect


* Date: 10/13/2022
  * Paired with Eric to work on some blockers concerning how we have our website pull up
    * need to have the whole page refresh when clicking on the home icon

* Date: 10/14/2022
  * Watched Dylan walk over how he utilized swagger to get the user account to be accessed
  * Technically we're done with our MVP~ we will start working on stretch goals after we figure out why Mimis swagger isn't working
  * Going to look more into Geolocation API info today
  * Dylan is still working on login/signup
  * Eric is going to practice writing unit tests
  * Will start read me soon
  * Eric and I paired and we were able to successfully get our page to reload
  * updated our logo to display instead of a carousel

* Date: 10/15/2022
  * Was able to successfully provide the location with lat and long
  * confirmed that lat and long work as arguments within the filter for location on rollform
  * added checkbox but checkbox needs to apply text to location

* Date: 10/17/2022
  * Pairing with Mimi to discuss how I can get the API to read the location info
  * Ended up working with a SEIR to work through getting our locationapi to be output inside of the location value input tag 
  * tomorrow will look to making a unit test for this somehow
  * cleared up some redundant code from index.css

* Date: 10/18/2022
  * Reading up on Unit tests today
  * look into applying a test to pull location for both positive, negative, and errored outcomes
  * unable to successfully complete making a unit test with Andrew 
  * Andrew said he would help me work on a unit test to "get" account tomorrow, first thing in the morning

* Date: 10/19/2022
  * Working with Andrew to help develop my unit test still
  * FINALLY FINISHED UNIT TEST FOR GETTING A FAKE ACCOUNT YAYY!!!!!
  * THIS THING TOOK WAY TOO LONG BUT WE GOT THROUGH IT WITH AN ASYNC FUNCTION THAT RETURNS THE FAKE ACCOUNT, HUZZAHHHH
  * worked on a few different things to style out our front end forms and buttons
  * learned about &nbsp;
  * learned about turnery operators in js


* Date: 10/20/2022
  * Worked on getting the password to not show up on the website
  * Looking to pair with dylan for favorites and blacklist so waiting on him resolving something with andrew for the backend
  * looking to try out another styled login logo instead of the shaking one to see how the group feels
  * might work with eric to add location related time to display open or closed note on project
  * unable to add hover effect properly, pushing it aside for now. I might require a different image type for it to show up
  * made the nav logo wiggle for fun
  

* Date: 10/21/2022
  * Pair programmed with Eric and Dylan for frontend API calls to post our favorites and blacklist of restaurants
  * Edited buttons with Eric on getting the buttons to look nicer for add to favorites and blacklists
  * considering the stretch goals of displaying if a restaurant is open or not locally

* Date: 10/22/2022
  * Edited buttons getting the buttons to look nicer for favorites and blacklists
  * Cleaned up Pipeline jobs
    * accidentally deployed but discovered 2 issues (my current theory is that it's related to the gitlab-ci.yml file)
      * login/signup/yelp api access is not fetching requests properly
      * requested advice from group, advised to return to those issues on monday as a group
    * discovered favicon.ico is apparently important? it was causing a failure in a pipeline job regarding our frontend deploy
    * created a favicon for our website

* Date: 10/23/2022
  * merged with main to get updated information on get requests for front end auth, apparently it's not functioning yet so we will go over it monday
  * main plans today are to study algorithms and data structures and for fun, maybe try to do something for the website so I don't feel useless
  * grew concern over charges from heroku, discovered the charges were based on dynos which had occurred since October 3rd so it wasn't directly my fault
  * requested to work with anyone available currently so that I can get some more experience in the program, people appear to be busy so I might just study algos I think?
  * I'm also considering looking more into the gitlab-ci file, but would really prefer to hear more about it from the instructors so I'll have to wait until monday or tuesday to deal with it, I believe it'll work parallel to us resolving the two issues I stated yesterday as well.
  * Created an issue in gitlab regarding a fetch request from the yelp api
    * if we can resolve the yelp one, I think we might be able to resolve the auth one, if not, we will probably hit up one of the instructors the day after or monday evening.
  * TDIL that dynos is based on usage and not based on how often we update the website.

* Date: 10/24/2022
  * took most of the day off due to not feeling well
  * Pair programmed with Carmen to style our favorites and dislikes list

* Date: 10/25/2022
  * Group programming in terms of creating and hooking up our database with MongoDB, followed directions via Learn but wasn't able to connect on the heroku app as of yet
  * Group programming in terms of linting our services and pipelines
  * Group programming to provide hair image to troll users
    * Potentially might change it later on?
      * Definitely going to RickRoll someone

* Date: 10/26/2022
  * deploying with group
  * bugs discovered - 
    * has to submit form twice
    * has to add at least one of each list to populate user list

* Date: 10/26/2022
  * Mimi had updated and cleaned some of the bugs like how we had to submit the form twice
  * added additional information to .env file so that we can access build locally
  * prepare for demo with Andrew

