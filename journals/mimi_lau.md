## MIMI LAU JOURNAL

## 10/26/2022:
- [x] Watched Dylan set up another app very nervously because lmao the dynos charge me
- [x] DEPLOYED!!!!!!!!!! DATABASE CONNECTED!!!!!! WE SEE THINGS!!!!
- [x] Went to lunch and came back and EVERYTHING BROKE
- [X] WHY
  - [X] HOW
    - [X] WHEN
      - [X] WHERE
        - [X] ?????????????????????????????????
- [x] Had to escalate to James, then escalated to Andrew because we couldn't figure it out. Andrew helped us out greatly by pointing out that the number of our arguments for a function was wrong. 
- [x] Everything worked.......except for one thing and that was on our form. For some reason, we had to enter the fields TWICE for the form to run. 
  - [x] Fixed it by removing a isMounted useRef. 
- [x] Fixed our user profile name not being grabbed correctly
  - [x] It would break our page on refresh
  - [x] Set a state and put it into the useEffect


## 10/25/2022:
- [x] Deployment H E L L
- [x] Why is the learn for connecting MongoDB SO SPARSE??? There is so little information we fumbled our way through it
- [x] Fixed deployment errors
  - [x] Error: no matching distribution found for psycopg.binary=3.0.14; extra =="binary"
  - [x] Removed binary from requirements
- [x] Added variables where they needed to be in the gitlab-ci file
- [x] ENCOUNTERED SOOOOOOOOOO MANY ERRORS I S2G I WILL YEET MYSELF OFF A CLIFF
  - [x] Turns out we needed to COPY the subdirectories of our service's directory 
  - [x] Dockerfile i hate you
- [x] Database still not connected


## 10/24/2022:
- [x] Continued to work on user profile for favorites/blacklist
- [x] Got help from James for grabbing information from the backend, but ran into an issue that he couldn't fix so he escalated to other instructrs
  - [x] lol...i don't recall ever solving this issue, but it went away 
- [x] Favorites/blacklist POPULATES!!! HOORAHHHHHHHHHHHHH MVP DONNNNNNNNE
  - [x] Some odd errors, like refreshing breaks it
  - [x] Fixed all the console errors that were screaming at me, including a key error and we didn't have unique keys for our dictionary
    - [x] Solved this by assigning an integer since we're mapping through everything anyway
- [x] Stressy wessy because we didn't deploy like we wanted to


## 10/23/2022:
- [x] Paired with Dylan and Carmen again to try again, but to no avail
  - [x] Even asked Robbie and Alex for help LOL we all dum
  - [x] Decided to escalate on Monday after the test because this was beyond our understanding at the moment


## 10/22/2022:
- [x] Paired with Dylan and Carmen to try and get backend GET methods for blacklist/favorites
- [x] Needed go get the actual account information for the front end
  - [x] Was only getting a success message??? 
  - [x] Changed the output of the backend to what is expected in the front end to test how the front end code should behave


## 10/21/2022:
- [x] Friday off.
- [x] Worried intensely for project and practice exam


## 10/20/2022:

Today, I worked on:
- [x] Cleared up some outstanding issues on gitlab
- [x] Helped Carmen clean up the file tree
- [x] This was the day that Jerid and Andrew came into our group...lol
  - [x] We did stand up and communicated and set a goal of deploying on Monday
  - [x] Delegated some backend work since Daniel said CSS doesn't count towards anything


## 10/19/2022:

Today, I worked on:
- [x] I wrote my unit test :)
  - [x] Random function returns random item from a list and it's not broken HUZZAH
- [x] Discovered we have to have separate databases for our microservices :(
- [x] Fixed up username to display on the actual user page
  - [x] Doesn't just display default "insert whatever here" lol
- [x] Tried to do a test of the yelp api and realized it's an integration test so nope, it doesn't work for this project :(


## 10/18/2022:

Today, I worked on:
- [x] I did battle with the nav bar and the login/logout modals
- [x] Ran into SO MANY unexpected behavior and bugs
  - [x] Wrapped the nav links in conditional statements so they display accordingly to logged in or logged out state
    - [x] THIS TOOK FOREVER BECAUSE WRAPPING IT IN CONDITIONALS SOMEHOW MADE ALL THE MODALS ACTIVATE
    - [x] Had to reset their state on every call because when you click them it doesn't actually unset them
  - [x] Fixed the login/signup modals so that when you click to activate them, it doesn't grey out and break the site
    - [x] Bootstrap is a menace
  - [x] Fixed the login/signup modals so that only one appears at a time instead of having them both appear when you click on them one after another
  - [x] Fixed login/signup modal behavior of clicking the X to close and the cancel button to close so it doesn't throw console errors
- [x] Unexpected (but known behavior) where hitting submit on the signup form creates an account regardless of putting in info
  - [x] Email validation done in regex!!!! HELL YEAH I LEARNED SOMETHING COOL


## 10/17/2022:

Today, I worked on:
- [x] Paired with Lee to try and get the geolocation data
- [x] Created the geolocation's checkbox logic to work with previous form logic
- [x] Learned about the ACTUAL way to use the token to get user data and not the way we were thinking of. :) Off to the backend...in other words, good luck Dylan LOL Gotta make those pydantic models to hold the favorites/blacklists
- [x] Researching unit tests


## 10/15/2022:

Today, I worked on:
- [x] Cleaned up Apps.js
- [x] Attempting to get log in user information from the backend to display on front end
- [x] Worked with Dylan for a bit to try and troubleshoot some code, but we need a SEIR LOL


## 10/14/2022: 

Today, I worked on:
- [x] Worked with Carmen to swap up some font styles
- [x] Fixed some CSS styles for other assorted things on the site
- [x] Added the yelp ratings with their patented stars and a yelp logo to footer! Things are looking nicer and more professional now!! 
- [x] Adding Curtis to the team...so our pipeline gets paid LOL


## 10/13/2022: 

Today, I worked on:
- [x] Created user profile page
- [x] Created black list/fav list tabs
- [x] Pipelines are gonna be paid??? Why :(
- [x] To be fair, I forgot to write the entry for this day and I think I was fighting with the footer's CSS for most of the day, but I blanked that out. Oh wait never mind, I remember--
- [x] FIXED FOOTER POSITIONING THAT WAS DUE TO FUNKY CSS AND FLOATING CONTAINERS. WOOHOO!!! THIS TOOK TOO LONG FOR WHAT IT ACTUALLY DETAILED


## 10/12/2022:

Today, I worked on:
- [x] Successfully merged to main and fixed all the conflicts
- [x] Ubiquitous languaged ourselves into understanding of the main landing page and the results page
- [x] PIPELINE GREEN!!!!!!!!!!!! AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA FINALLY FIXED THE FRONT END ISSUES SO IT GOES THROUGH
- [x] Form now has term as optional; if there's no search term, it's defaulted as food.
- [x] Prevented useEffect from running on mount; don't want our effects to run right at the beginning.
- [x] Restored strict mode
- [x] Added flake8 test QA to gitlab-ci.yml


## 10/11/2022: 

Today, I worked on:
- [x] Watched Dylan try to troubleshoot and work with authentication 
- [x] Passing data to another functional component!! WOOHOO TY SEIRS
- [x] Created front-end visualization of yelp restaurant data
- [x] Fixed form width
- [x] Centered carousel


## 10/10/2022:

Today, I worked on:
- [x] Had no power for most of the day
- [x] Andrew got us into our mongo database!!! WOHOO
  - [x] Turns out we had to open it in a new terminal window
  - [x] docker exec -it <<container number>> bash
  - [x] mongo -u root -p password --authenticationDatabase admin
  - [x] now try db.getCollection()


## 10/08/2022:

Today, I worked on:
- [x] Mongosh doesn't work :( Why!!
- [x] Tried authentication but couldnt get it to do anything...again
- [x] Created a queries file
  - [x] Created client.py


## 10/07/2022:

Today, I worked on:
- [x] Set up MongoDB as per instructions on Learn
  - [x] Doing anything in mongo right now results in authentication error
  - [x] Tried using mongosh to access anything in db
- [x] Looked into axios
- [x] Researching MongoDB


## 10/06/2022: 

Today, I worked on:
- [x] Worked with Carmen over figuring out filters for yelp data
- [x] Tried using categories, but it kept returning really weird data. So in a mutual decision, we decided to just comment it out for now. 
  - [x] Weird data like searching for ramen and returning super markets.
- [x] WROTE A SUCCESSFUL TRY/EXCEPT THAT CAUGHT THE EXCEPTION!!!!!!! WOOOHOOO
- [x] LEARNED HOW TO MERGE TO MAIN AND MERGE TO BRANCH! :D :D :D
- [x] Renamed sample_service to high_table


## 10/05/2022:

Today, I worked on:
- [x] Talked about MongoDB vs PostgreSQL and we decided on using MongoDB.
- [x] Created a testing endpoint for getting yelp api data
  - [x] WE CAN GET DATA!!!!!!!!!! WOOHOO
  - [x] Encountered some issues with filtering and getting the results we want; ie, we're a restaurant/food app, but if someone wants to search up makeup, we can't stop them. However, it does give really odd results. 
- [x] Created a random function that could randomize arguments passed in


## 10/04/2022:

Today, I worked on:
- [x] Trying to get docker image up and running; was still exiting with code 1 and I tried deleting volumes/containers/images. Nothing works and error still persists. :(
- [x] Andrew came in to help; deleted the node_modules file and did docker-compose build --no-cache
- [x] DOCKER IMAGE WORKS!!! Turns out it was corrupted files in the node_modules when docker was doing its thing. WOOHOOOOOO
- [x] Created branches
- [x] Pushed journal to branch
- [x] Created .gitattributes file and configured it according to Project Setup in learn
- [x] Discussed between MongoDB vs PostgreSQL; we haven't really been introduced to MongoDB yet (it will be introduced tomorrow in class) so I'm currently watching YT videos to hopefully better understand 


## 10/03/2022: 

Today, I worked on:
- [x] Created heroku account for deploying the project template
- [x] Got help to clarify Heroku Variables/Cors_Host
   - [x] CORS_HOST: "https://USERNAME.gitlab.io"
- [x] Setting CI/CD Variables
- [x] Pushed to main, got errors on the pipeline
   - [x] Fixed pipeline error with Daniel's help; the gitlab-ci.yml file had crucial lines commented out
- [x] Went to https://c_p.gitlab.io/module-3-project-gamma/ to check if site is up
- [x] Had an problem returning undefined where there was double // within the get request URL
- [x] Created issue ticket in gitlab for the above problem
   - [x] Fixed error in gitlab-ci.yml
   - [x] Closed issue ticket
- [x] https://c_p.gitlab.io/module-3-project-gamma/ SITE WORKS! Displays the due date!!!! WOOHOO
- [x] Tried to docker-compose build/up and docker image fails to start
   - [x] Error message: "Unknown command: "start"" 
   - [x] Exits with code 1
   - [x] Tried to fix by changing CRLF to LF for the run.sh file, but it didn't work