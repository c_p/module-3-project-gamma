## Eric (Yu) Lu Journal


### Journal format/template
```
* Date: 
* A list of features/issues that you worked on and who you worked with, if applicable
* A reflection on any design conversations that you had
* At least one ah-ha! moment that you had during your coding, however small
```

### Date: 10/20/2022 & 10/21/2022
### Todo list 
  [ ] Create [Get] endpoints for fav and unfav lists once Dylan get the backend setup. 

* A list of features/issues that you worked on and who you worked with, if applicable
  * Resolve some confilcts with the team, the team gets much stronger by doing: 
    * Better communication, information transparency. 
    * Task update based on status. 
    * Daily Stand ups. 
    * Quick response on slack or anyother communication platform.  
  * Got some really valuable actionable feedback from Carmen for my personal growth in a professional workplace.  
    * To take more initiative to take responsibility during work assigning discussion. 
    * Did good job on tasks have been assigne to me.
    * It's ok to request another teammates to work on together for a task I am highly intested in. 
  * POST endpoints for fav and unfav lists. working with Lee with Dylan and Andrew's support. 
  * CSS styling the adds buttons working with Lee. 

* A reflection on any design conversations that you had
  * Mimi and Carmen provide a great opportunity for me to work on some frontend endpoints and encourge me to work with Dylan to have better understanding the backend and database stuff and contribution to our project. 
* At least one ah-ha! moment that you had during your coding, however small
  * Don't overthink when attemptting to get a feacture to be functional. 





### Date: 10/19/2022 
* A list of features/issues that you worked on and who you worked with, if applicable
  * Suggested the team to wrap up what we got so far
  * Databata spliting (Dylan) WIP -- blocking to proceed with deployment prep. 
    * need to double check with Dylan if he needs help
  * Finished unit test for get_token None test case. 
  * CSS for landing page and the form; live coding with Lee. 
* A reflection on any design conversations that you had
  * Professionally consistentcy in communication within the team needs to work on in order to deliever the product on time.
  * Suggested the team to have standups after morning problem pratice. Last standups was on 10-14.
  * Plan for tmr: 
    * Pre-study on deployment stuff. 
    * live code cleaning with the team.    
* At least one ah-ha! moment that you had during your coding, however small
  * unable to have multiple hover effects at the same time??? might need to spend more time on finding out the answer for that. 
### Date: 10/18/2022 
* A list of features/issues that you worked on and who you worked with, if applicable
  * Working on README.md 
  * Working on unit test, got some blockers. 
* At least one ah-ha! moment that you had during your coding, however small
  * some diagram during planning might not be useful for readme since scope has been pivoted while develping a project. 

### Date: 10/17/2022 
* A list of features/issues that you worked on and who you worked with, if applicable
  * Cover Page Image hover effect improvement
  * Went through project advice to review the all the requirments.
    * noticed that each person on the team have to write at least unit test. 
    * Shared this info with the group
* A reflection on any design conversations that you had
  * I was thinking if we need a one more info open_now for the result page. 

* At least one ah-ha! moment that you had during your coding, however small
  * unit test can be written for each def function ... need more info. 
  * planning to start writing unit test
### Date: 10/14/2022 
* A list of features/issues that you worked on and who you worked with, if applicable
  * Merge before EOD.
  * sync with the team, and listed all the to-do based on the priority
  * ### P0:

   [x]  Merging conflicts
   [ ]  Wrap up login / sign up
   [ ]  Start on Readme
       more diagrams? maybe?
   [ ]  Figure out deployment
   [x]  Might write unit test for endpoints.
   [x]  CSS for buttons

## P1:

   [ ]  Nav bar links
    - all the endpoints are ready?
   [ ]  Continue front-end cleaning up (watch out for Mimi's missing divs)
   [ ]  Clean up code (print statements, console.log, unused imports)

### P2:

   [ ]  Profile page (SG)
    - Add restaurants to Favorite / Blacklist
    - Roll from Favorites only button

* A reflection on any design conversations that you had
  * Feature might not be working after merge. Need to keep testing out the feature for each merge. 
* At least one ah-ha! moment that you had during your coding, however small
  * Project management skill is playing a important role to work with dependencies. 
### Date: 10/13/2022 
* A list of features/issues that you worked on and who you worked with, if applicable
  * Finished cover page for our MVP! Wahoo~~~
  * Merge to the main, resolve merge conflicts. 
  * Fixed small bugs after merging.
* A reflection on any design conversations that you had
  * Good communication with group for merging is the key
* At least one ah-ha! moment that you had during your coding, however small
  * Feature might not be functional after merge to main branch.
### Date: 10/12/2022
* A list of features/issues that you worked on and who you worked with, if applicable
  * Sync with team to re-arrange components -- moving MainPage.js to "main" tag from "header" tag
  * worked on Cover page for our mvp. 
    * it is functional: load the page, it would show the cover page, then user clicks on anywhere on the main area, cover page will go away and show the form (MVP).  
* A reflection on any design conversations that you had
  * There are so many different ways to accomplish to the goal. It's really imporant to communicate with the team. 
* At least one ah-ha! moment that you had during your coding, however small
  * one single small feature seems easy to do, but it might take hours to figure it out. 

### Date: 10/11/2022 
* A list of features/issues that you worked on and who you worked with, if applicable
  * sync with Lee to assign the work for front end js file. 
  * collabrate with Dylan to figure out Mongo DB express. 
  
* A reflection on any design conversations that you had
  * Not feeling 100% today. My teammates are extremely supportive, so I could take a few hours of recharge.
* At least one ah-ha! moment that you had during your coding, however small
  * Indentation matters for docker compose yaml file for mongo express. 
  * Useful cmd dealting with mongo express setting up. 


  ```
  docker compose down
  docker compose build 
  docker compose up 
  docker container prune -f 
  docker image prune -a then => Y
  docker volume create mongo-data
  ``` 


### Date: 10/10/2022
* A list of features/issues that you worked on and who you worked with, if applicable
  * Created data flow diagram for high table project issue#9
  * Issue Creation for sign-up issue#8/ login issue#10 
  * Documented a blockage and resolution for mongoDB issue issue#7
  * Run docker compose location and fix api key issue by creating env file and adding my yelp api key in the file    
* A reflection on any design conversations that you had
  * Need to expand knowledge on mongoDB
* At least one ah-ha! moment that you had during your coding, however small
  * issue list, some issue are highlighted in green, just noticed that those are in "todo" status. 


### Date: 10/05/2022
* A list of features/issues that you worked on and who you worked with, if applicable
  * Created UML use case with the team to make sure everyone is on the same page. 
  * Played with Yelp API 
* A reflection on any design conversations that you had
  * During UML use case diagram process, noticed that the team had original two path for login user feature. With the diagram, the team speaks Ubiquitous Language now. 
* At least one ah-ha! moment that you had during your coding, however small
  * FARM stack for this project . FastAPI, React, MongoDB. 

### Date: 10/04/2023

* A list of features/issues that you worked on and who you worked with, if applicable
    * worked with the group and observed one     team member trouble shooting docker compose issue. 
  
    * Docuemented troubleshooting steps and resolution. see detail at https://arrow-airmail-b3f.notion.site/Stand-Ups-7a4204e3166e48399ec434d183d42443
    * Documented steps on how to deploy our project see detail at https://arrow-airmail-b3f.notion.site/Deployment-988abb43b4b04519a39284ea43f4db51


* A reflection on any design conversations that you had
  * had conversation with team and the class about deploying our project on heroku vs AWS since heroku is going to start charing in Nov. 


* At least one ah-ha! moment that you had during your coding, however small
  * Using Java might be more pratical to deploy project on AWS for security and maintainance purpose. 
