# Project Gamma

  ### High Table - Restaurant Randomizer

# Project Description
  - Yelp API related project dealing with FastAPI, React, JavaScript, Python, JSX, AJAX, docker,Mongo DB, Microservices, and deployment via Heroku and Git
  - Provides a selected restaurants from a randomized group of restaurants within the parameter of arguments provided. 
  - Users are able to create an account in order to have functionality of creating own preference lists : Favorite List and Dislike List. 
  - Public site: https://c_p.gitlab.io/module-3-project-gamma

# Table of Contents
  [[_TOC_]]

# How to Install and Run the Project

## Getting started: 

### 1. Getting Yelp API key 
  - Visit https://www.yelp.com/developers/documentation/v3/authentication 
  - Follow the instructions and get Yelp API key 
  - Example YELP API key (replace * to have working API key) 

  ```
    amh3onJAznnH-Gl9XQwSTqunBCeGPIXzNuibh3-oZuUne_nDYM-*******************************-3E8TIld5tGFYry38DuI0Y3Yx
  ```
### 2. Creating .env file 
  - Create a file and name it as .env, then inject YEIP_API_KEY  

  ![image](/uploads/9d90d36ddfb52b8470249f4eb46f25c5/image.png)

  
  ```
    YELP_API_KEY="api key string here"
  ```

### 3. Run Docker
  - Use Command:
  ```
    docker volume create mongo-data
    docker-compose build
    docker-compose up
  ```

### 4. Visit localhost:3000 in browser
  - On the page, click to remove landing page to use the Restaurant Randomizer. 

# How to Use the Project 

  ### landing page demo

  ![landing page demo](http://g.recordit.co/ZFOnEMpQol.gif)
  - MVP - On form, LOCATION field is required, all other fields optional.
  - If you have loaction enabled, then you can check use current locatioin for the LOCATION field input. 
  - Enter in location and optional fields and hit Random button!
  
  ### MVP gif demo

  ![MVP demo](http://g.recordit.co/0neBOifoU5.gif)

  ### Login demo

  ![Logindemo](http://g.recordit.co/i5q7lfnJaq.gif)

  ### Logout demo 

  ![Logout](http://g.recordit.co/y491J81BIb.gif)
  
  ### Profile demo

  ![profilepage](http://g.recordit.co/iLAkgSWRX6.gif)


# Credits
  ## SJP Jul 2022 PT Team 5: C & P

  * [Carmen Tang](https://gitlab.com/kanpii)
  * [Dylan Thacker](https://gitlab.com/dylthac)
  * [Eric Lu](https://gitlab.com/ericlu9256)
  * [Lee Pham](https://gitlab.com/LeeNPham) 
  * [Mimi Lau](https://gitlab.com/mlau1137)
     

# Project Diagram


## Data Flow Diagram

![data flow chart](/uploads/06482cb48a3e65a42dbf4b75d29988ad/image.png)

## Use Case Diagram

![Use case](/uploads/7eb0cddadbf5715a6b19c90f887d2689/image.png)

## API desgin
[API Endpoint Tempalte](docs/api-design.md)


