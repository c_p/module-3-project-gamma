from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import accounts

import os

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", "REACT_APP_API_HOST"),
    "http://localhost:3000",
]

app.include_router(authenticator.router)
app.include_router(accounts.router)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
