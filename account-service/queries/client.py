import os
import pymongo

# client_heroku = pymongo.MongoClient(os.environ["MONGODB_URI"])
# MONGODB_URI = os.environ["MONGODB_URI"]

client = pymongo.MongoClient(os.environ["DATABASE_URL"])
# dbname = os.environ['MONGODATABASE']
db = client["mongo-data"]


class Queries:
    @property
    def collection(self):
        db = client[self.DB_NAME]
        return db[self.COLLECTION]
