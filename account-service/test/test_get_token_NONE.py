from queries.accounts import AccountOut
from main import app
from fastapi.testclient import TestClient
from authenticator import authenticator

client = TestClient(app)


fakeAccOut = AccountOut(
    id="1", email="4444email@email.com", username="username", roles=["string"]
)

fakeAccToken_none = None


async def account_out_override():
    return fakeAccOut


app.dependency_overrides[
    authenticator.try_get_current_account_data
] = account_out_override


def test_get_account():
    # This cookie_name is not in fastapi authentication cookies:
    # In jwtdown_fastapi authentication, it sets cookie_name: str = "fastapi_token" in init.
    response = client.get("/token", cookies={"test_token": ""})
    assert response.status_code == 200
    # get_token function will return None when cookie_name is not in request.cookies
    assert response.json() == fakeAccToken_none
