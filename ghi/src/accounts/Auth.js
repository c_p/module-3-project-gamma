import { createContext, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
let internalToken = null;

export function getToken() {
  return internalToken;
}

export async function getTokenInternal() {
  const url = `${process.env.REACT_APP_AUTH_HOST}/token`;
  try {
    const response = await fetch(url, {
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      internalToken = data.access_token;  //data.account
      return [internalToken, data.account]
    }
  } catch (e) {}
  return false;
}

// function handleErrorMessage(error) {
//   if ("error" in error) {
//     error = error.error;
//     try {
//       error = JSON.parse(error);
//       if ("__all__" in error) {
//         error = error.__all__;
//       }
//     } catch {}
//   }
//   if (Array.isArray(error)) {
//     error = error.join("<br>");
//   } else if (typeof error === "object") {
//     error = Object.entries(error).reduce(
//       (acc, x) => `${acc}<br>${x[0]}: ${x[1]}`,
//       ""
//     );
//   }
//   return error;
// }

export const AuthContext = createContext({
  token: null,
  setToken: () => null,
  account: null,
  setAccount: () => null,
});

export const AuthProvider = ({ children }) => {
  const [token, setToken] = useState(null);
  const [account, setAccount] = useState(null);

  return (
    <AuthContext.Provider value={{ token, setToken, account, setAccount }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);

export function useToken() {
  const { token, setToken, account, setAccount } = useAuthContext();
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchToken() {
      const getTokenResponse = await getTokenInternal();
      if (getTokenResponse === false){
        return
      }
      const [token, account] = getTokenResponse;
      setToken(token);
      setAccount(account);
    }
    if (!token) {
      fetchToken();
    }
// setAccount and account were added by Lee, potential take this off if it creates any issues. Just trying to resolve pipeline error 
  }, [setToken, token, setAccount, account]);

  async function logout() {
    if (token) {
      const url = `${process.env.REACT_APP_AUTH_HOST}/token`;
      await fetch(url, { method: "delete", credentials: "include" });
      internalToken = null;
      setToken(null);
      setAccount(null);
      navigate("/");
    }
  }

  async function login(username, password) {
    //default was /login/ is that needed when all endpoints end in /token?
    // const url = `${process.env.REACT_APP_API_HOST}/login/`;
    const url = `${process.env.REACT_APP_AUTH_HOST}/token`;
    const form = new FormData();
    form.append("username", username);
    form.append("password", password);
    const response = await fetch(url, {
      method: "post",
      credentials: "include",
      body: form,
    });
    if (response.ok) {
      const getTokenResponse = await getTokenInternal();
      if (getTokenResponse === false){
        return
      }
      const [token, account] = getTokenResponse;
      setToken(token);
      setAccount(account);
      return;
    }
    // lpham- uncommented the line for error since someone already commented the function that uses parameter "error"? 
    // let error = await response.json();
    // return handleErrorMessage(error);
    navigate("/");
  }

  async function signup(username, password, email, firstName, lastName) {
    const url = `${process.env.REACT_APP_AUTH_HOST}/api/accounts`;
    const response = await fetch(url, {
      method: "post",
      body: JSON.stringify({
        username,
        password,
        email,
        first_name: firstName,
        last_name: lastName,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      navigate("/");
    }
    return false;
  }

  async function update(username, password, email, firstName, lastName) {
    const url = `${process.env.REACT_APP_AUTH_HOST}/api/accounts`;
    const response = await fetch(url, {
      method: "post",
      body: JSON.stringify({
        username,
        password,
        email,
        first_name: firstName,
        last_name: lastName,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      // await login(username, password);
    }
    return false;
  }

  return [token, login, logout, signup, update, account];
}