import React from 'react';
import { useState } from 'react';
import { useToken } from './Auth';
import '../Modal.css';

function BootstrapInput(props) {
    const { id, placeholder, labelText, value, onChange, type } = props;

    return (
        <div className='mb-4'>
            <label htmlFor={id} className='form-label'>{labelText}</label>
            <input value={value} onChange={onChange} type={type} className='form-control' id={id} placeholder={placeholder}/>
        </div>
    );
}

function SignupModal({ closeLogout, closeSignup, closeLogin }) {
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [,,, signup] = useToken();

    const handleSubmitValidation = (e) => {
        e.preventDefault();
        //do not stringify this regex
        //some simple explanation feel free to move to notes if you want
        //^ asserts position at the start of the string
        //[a-z0-9.] means matching a single character presented in the list
        //a-z, match one between this
        //0-9 match between this
        //. matches the character '.' cuz sometimes you have '.' in emails
        //{1,64} match the previous between 1-64 times, as many times as possible
        //@ matches the '@' cuz all emails have this
        //$ asserts position at the end of the string
        //i means case insensitive match, ignoring upper/lower case
        //for more info, check out regex101.com
        const regexEmail = /^[a-z0-9.]{1,64}@[a-z0-9.]{1,64}$/i;
        if (!regexEmail.test(email)) {
            return alert("Email is invalid")
        }
        else if (password==="") {
            return alert("Password is invalid")
        }
        else if (username==="") {
            return alert("Username cannot be blank")
        } 
        else {
            signup(username, password, email);
            setUsername('')
            setPassword('')
            setEmail('')
            closeSignup(false)
            alert("Account created! Login using your created credentials.")
        }
    }

    return (

        <div className="modalBackground" id='Modal'>
            <div className="modalContainer">
                <div className="titleCloseBtn">
                    <button
                        onClick={() => {
                            closeSignup(false);
                            closeLogout(false);
                            closeLogin(false);
                        }}
                    >
                        X
                    </button>
                </div>
                <div className="title">
                    <h1>Signup for an account</h1>
                </div>
                <form>
                    <BootstrapInput
                        id='email'
                        placeholder='youremail@example.com'
                        labelText='Your email address'
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        type='email' />
                    <BootstrapInput
                        id='name'
                        placeholder='Blah Blah'
                        labelText='Your username'
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        type='text' />
                    <BootstrapInput
                        id='password'
                        placeholder='Your secret password'
                        labelText='Password'
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type='password' />
                </form>
                <div className="footer">
                    <button
                        onClick={() => {
                            closeSignup(false);
                            closeLogout(false);
                            closeLogin(false);
                        }}
                        id="cancelBtn"
                    >
                        Cancel
                    </button>
                    <button
                        onClick={handleSubmitValidation} type='submit' className='btn btn-primary'>Submit</button>
                </div>
            </div>
        </div>
    );
}


export default SignupModal;


