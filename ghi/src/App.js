import './App.css';
import RollForm from './high_table/RollForm.js';
import Nav from './high_table/Nav.js';
import Footer from './high_table/Footer.js'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AuthProvider, useToken } from './accounts/Auth'
import UserProfile from './high_table/UserProfile';


export function GetToken() {
  useToken();
  return null
}

// using RollForm as our home page
function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <AuthProvider>
      <BrowserRouter basename={basename}>
        <GetToken />
        <Nav />
        <Routes>
          <Route path="/" element={<RollForm />} />
          <Route path="/user" element={<UserProfile />} />
        </Routes>
        <Footer /> 
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
