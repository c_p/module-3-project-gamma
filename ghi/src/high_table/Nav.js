import React from "react";
import { NavLink } from "react-router-dom";
import { useState } from 'react';
import LoginModal from "../accounts/LoginModal";
import SignupModal from "../accounts/SignupModal";
import LogoutModal from "../accounts/LogoutModal";
import { useToken } from "../accounts/Auth";
import '../Modal.css'
import '../index.css';


function Nav() {
    const [openLogin, setOpenLogin] = useState(false)
    const [openSignup, setOpenSignup] = useState(false)
    const [openLogout, setOpenLogout] = useState(false)
    const [token] = useToken();
    // using the refreshPage function to allow user clicking on logo to visit the home page with our cover page.    
    function refreshPage() {
        window.reload(false);
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark" >
            <div className="container-fluid">
                                                {/* please don't hate me, i made our logo button on the top left, wiggle wiggle wiggle */}
                <NavLink className="navbar-brand jm-logo" onClick={refreshPage} to="/">
                    <img id="logo" alt="" width="60px" height="auto" src={'HTGRAY.gif'} />
                </NavLink>
                <div className="container-fluid">
                </div>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-2">
                        {token ? (
                        <>
                        <div className="button">
                            <button
                                className="btn btn-light mx-3" type="button" id="dropdownMenuButton1" data-bs-target='#loginModal' aria-hidden='true' tabIndex='-1'
                                onClick={() => {
                                    setOpenLogout(true);
                                    setOpenLogin(false);
                                }}
                            >
                                Logout
                            </button>
                            {openLogout && <LogoutModal closeLogout={setOpenLogout} />}
                        </div>
                        <div className="button">
                            <button className="btn btn-light mx-3" type="button" id="profileButton" aria-expanded="false">
                                <NavLink to="/user">Profile</NavLink>
                            </button>
                        </div>
                        <div>
                            <img width="60px" alt="" height="auto" src={'profileimg.jpg'} />
                        </div>
                        </>) : 
                        (
                        <>
                            <div className="button">
                                <button
                                    className="btn btn-light mx-3" type="button" id="dropdownMenuButton1" data-bs-target='#loginModal' aria-hidden='true' tabIndex='-1'
                                    onClick={() => {
                                        setOpenLogin(true);
                                        setOpenSignup(false);
                                        setOpenLogout(false);
                                    }}
                                >
                                    Login
                                </button>
                                {openLogin && <LoginModal closeLogin={setOpenLogin} />}
                            </div>
                            <div>
                                <button
                                    className="btn btn-light mx-3" type="button" id="dropdownMenuButton1" data-bs-target='#loginModal' aria-hidden='true' tabIndex='-1'
                                    onClick={() => {
                                        setOpenSignup(true);
                                        setOpenLogin(false);
                                    }}
                                >
                                    Signup
                                </button>
                                {openSignup && <SignupModal closeSignup={setOpenSignup} />}
                            </div>
                        </>)}
                    </ul>
                </div>
            </div>
        </nav>
    )
}



export default Nav;
