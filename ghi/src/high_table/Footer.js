import React from 'react';
import '../index.css';

function Footer() {
    return (
        <>
            <footer className="footer m-5" id="footer">
                <div className="d-flex flex-column flex-sm-row justify-content-between">
                    <p>Built by C&P, Team 5 of Eric, Mimi, Lee, Carmen, and Dylan © 2022 C&P, Inc. All rights reserved.</p>
                    <img src={require(`../images/yelp_logo.png`)} width="5%" height="5%" alt=""/> 
                </div>
                <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" className = "my-0 py-5 float-child-top" target="_blank" rel="noreferrer">
                    <img src={require(`../images/hair_on_screen.png`)} width="125px" height="auto" alt="" />
                </a>
            </footer>
        </>
    )
}

export default Footer;