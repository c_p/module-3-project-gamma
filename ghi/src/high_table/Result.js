import { useEffect, useState } from "react";
import { useToken } from '../accounts/Auth'
import axios from "axios";

// Will need to replace "random string" on lines 15 & 29 with the restaurant name
// so we can start populating the lists. Try restaurant.data.name? Could be something else?
// Or add another parameter other than token on lines 11 & 25, then set that param
// to the key on lines 15 & 29

//Already populates the database depending on current username, just need to save the correct data.

//Are the headers needed? It worked fine at first, but no cookie is showing up in network tab.
async function addFavorite(token, favorite) {
    const favUrl = `${process.env.REACT_APP_API_HOST}/favorite`
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify({ "favorite": favorite }),
        headers: {
            Authorization: `Bearer ${token}`,
            credentials: "include",
            accept: "application/json",
            "Content-Type": "application/json"
        }

    };
    await fetch(favUrl, fetchConfig);
}

async function addBlacklist(token, blacklist) {
    const favUrl = `${process.env.REACT_APP_API_HOST}/blacklist`
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify({ "blacklist": blacklist }),
        headers: {
            Authorization: `Bearer ${token}`,
            credentials: "include",
            accept: "application/json",
            "Content-Type": "application/json"
        }
    };
    await fetch(favUrl, fetchConfig);
}

function RestaurantDisplay(restaurant, token) {
    

    return (
        <>
            <p><font size="6">{restaurant.data.name}</font></p>
            <img src={restaurant.data.image_url} alt="" id="yelp_img" />
            <p>{restaurant.data.display_phone}</p>
            <p>{restaurant.data.location.display_address[0]}
                <br />{restaurant.data.location.display_address[1]}
                <br />{restaurant.data.location.display_address[2]}</p>
            <p>{restaurant.data.price}</p>
            <img
                src={require(`../images/${restaurant.data.rating}.png`)}
                alt={`Yelp ratings: ${restaurant.data.rating}/5`} />
            <br /><a href={restaurant.data.url} target="_blank" rel="noreferrer" >Link to Yelp</a>
            <div className="button">
                {token ? <><button
                    onClick={() => {
                        addFavorite(token, restaurant.data.name);
                    }} type='submit' className='button23 mx-3 my-2'>Add to Favorites</button>
                    <button
                        onClick={() => {
                            addBlacklist(token, restaurant.data.name);
                        }} type='submit' className='button23 mx-3 my-2'>Never Show Again</button></> :
                    <></>}

            </div>
        </>
    )
}

export default function Result(props) {
    const { data } = props
    const [yelpData, setYelpData] = useState();
    const [token] = useToken();
    // const isMounted = useRef(false)

    useEffect(() => {
        async function forData() {
            if (data.term === "") {
                data.term = "food"
            }
            if (data.price) {
                const axiosDataPrice = await axios.get(`${process.env.REACT_APP_API_HOST}/api/yelp-test?location=${data.location}&term=${data.term}&price=${data.price}`)
                setYelpData(axiosDataPrice)
            } else {
                const axiosData = await axios.get(`${process.env.REACT_APP_API_HOST}/api/yelp-test?location=${data.location}&term=${data.term}`)
                setYelpData(axiosData)
            }
        }
        forData();
    }, [data])


    return (
        <>
            {yelpData ? RestaurantDisplay(yelpData, token) : (null)}
        </>
    )
}
