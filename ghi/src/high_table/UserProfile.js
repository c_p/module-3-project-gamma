import { useEffect, useState } from "react";
import { useToken, useAuthContext } from '../accounts/Auth'

function UserProfile() {
    const [token] = useToken();
    const [ favoritesData, setFavoritesData ] = useState([]);
    const [ dislikesData, setDislikesData ] = useState([]);
    const [ accountData, setAccountData ] = useState([])
    const {account} = useAuthContext();
    // console.log(account)
    useEffect(() => {
        async function GetPrefs() {
            const prefsUrl = `${process.env.REACT_APP_API_HOST}/getfavorite`;
            const fetchConfig = {
                method: 'GET',
                headers: { Authorization: `Bearer ${token}`},
                credentials: "include",
            };
            const response = await fetch(prefsUrl, fetchConfig);
            const dislikes_response = await fetch(`${process.env.REACT_APP_API_HOST}/getblacklist`, fetchConfig)
            if (response.ok && dislikes_response.ok) {
                const data = await response.json();
                const dislikes_data = await dislikes_response.json();
                setDislikesData(dislikes_data.blacklists);
                setFavoritesData(data.favorites);
                setAccountData(account.username);
            }
        }
        if (token) {
            GetPrefs();  
        }
    }, [token, account])


    return (
        <>
        <div className="m-5 p-5">
            <div className="col-md-6">
                <h3>
                    Profile Preferences: {accountData}
                </h3>
            </div>
            <nav>
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                    <button onClick={() => {
                        }} className="button24 mx-3 my-2 nav-link active" id="favorites" data-bs-toggle="tab" data-bs-target="#nav-favorites" type="button" role="tab" aria-controls="nav-favorites" aria-selected="true">Favorites</button>
                    <button className="button24 mx-3 my-2 nav-link" id="dislikes" data-bs-toggle="tab" data-bs-target="#nav-dislikes" type="button" role="tab" aria-controls="nav-dislikes" aria-selected="false">Dislikes</button>
                </div>
            </nav>
            <div className="tab-content" id="nav-tabContent">
                <div className="tab-pane fade show active" id="nav-favorites" role="tabpanel" aria-labelledby="favorites">
                    <table>
                        <tbody>
                            {favoritesData.map((favs, i) => {
                                return (
                                    <tr key={i}>
                                        <td>{favs.favorite}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="tab-pane fade" id="nav-dislikes" role="tabpanel" aria-labelledby="dislikes">
                    <table>
                        <tbody>
                            {dislikesData.map((dis, i) => {
                                return (
                                    <tr key={i}>
                                        <td>{dis.blacklist}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    );
}

export default UserProfile;