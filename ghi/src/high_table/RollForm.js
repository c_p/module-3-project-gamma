import { useEffect, useState } from "react";
import Result from "./Result";
//import mainpage == cover page of our website
import MainPage from './MainPage.js'
//Fade in & Fade out effect
import FadeInOut from "./FadeOut";

function BootStrapInput(props) {
    const { id, placeholder, labelText, value, onChange, type } = props;
    if (id === 'location') {
        return (
            <div className="mb-3 p-3">
                <label htmlFor={id} className="form-label">{labelText}</label>
                <input value={value} onChange={onChange} type={type} className="form-control" id={id} placeholder={placeholder} required />
            </div>
        )
    }
    return (
        <div className="mb-3 p-3">
            <label htmlFor={id} className="form-label">{labelText}</label>
            <input value={value} onChange={onChange} type={type} className="form-control" id={id} placeholder={placeholder} />
        </div>
    )
}


function RollForm() {
    const [term, setTerm] = useState("");
    const [location, setLocation] = useState("");
    const [data, setData] = useState({});
    const [isTrue, setIsTrue] = useState(false);
    const [price, setPrice] = useState("");
    // this is for our cover page, initial as True
    const [cover, setCover] = useState(true);
    const [position, setPosition] = useState(null);
    const [checkBox, setCheckBox] = useState(false);
    

    const handleSubmit = (e) => {
        e.preventDefault();
        setData({ term: term, location: location, price: price });
        setTerm("");
        setLocation("");
        setPrice("");
        setCheckBox(false);
        setIsTrue(true);
    }

    const handleCheck = (e) => {
        if (e.target.checked) {
            setLocation(position);
            setCheckBox(!checkBox);
        } else {
            setLocation("");
            setCheckBox(!checkBox);
        }
    }

    // This is the thing that allows you to grab your location
    useEffect(() => {
        const successCallback = (position) => {
            setPosition(`${position.coords.latitude}, ${position.coords.longitude}`)
        };

        const errorCallback = (error) => {
            console.error(error)
        };

        navigator.geolocation.getCurrentPosition(successCallback, errorCallback, {
            enableHighAccuracy: true,
            timeout: 5000
        });
    }, [position])

    // this is onClick event.... clink anywhere on our main area, cover will be go away and form will show up. 

    const handleonClick = (e) => {
        setCover(false)
    }
    let show = true;

    let coverPage =  (
        <div className="cover-page" onClick={handleonClick}>
            <FadeInOut show={show} duration={3000} >
                <MainPage />
            </FadeInOut>
        </div>
    )
    if (cover) {
        return coverPage
    }


    return (
        <div className="float-container">
            <div className="form-style-9" id="form">
                <form onSubmit={handleSubmit}>
                    <BootStrapInput
                        id="term"
                        placeholder="Food Type Here"
                        labelText="Your Food Choice"
                        value={term}
                        onChange={e => setTerm(e.target.value)}
                        type="text" />


                    {position ? (
                    <>
                    <BootStrapInput
                        id="location"
                        placeholder="Location Here"
                        labelText="Where are you?"
                        value={location}
                        onChange={e => setLocation(e.target.value)}
                        type="text" />
                    <div className="input-group">
                        <div className="input-group-text p-2">

                            <input type="checkbox" checked={checkBox} onChange={handleCheck} aria-label="Checkbox for following text input" />
                                <span>&nbsp; Use Current Location</span>
                        </div>
                    </div>
                    </>): 
                    (<BootStrapInput
                        id = "location"
                        placeholder = "Location Here"
                        labelText = "Where are you?"
                        value = { location }
                        onChange = {e => setLocation(e.target.value)}
                        type="text" />)}



                    <div className="mb-3 p-3">
                        <label htmlFor="Price Range" className="form-label">Choose Your $ Price $</label>
                        <select value={price} onChange={e => setPrice(e.target.value)} className="form-select" aria-label="Choose Your Price">
                            <option>Open to Select $</option>
                            <option value="1">$</option>
                            <option value="2">$$</option>
                            <option value="3">$$$</option>
                            <option value="4">$$$$</option>
                        </select>
                    </div>
                    <div className="mb-3 p-3">
                        <button type="submit" className="button-cus">Randomly Feed Me</button>
                    </div>
                </form>
            </div>
            <div className="m-3 p-3 w-50 float-child" id="result">
                {isTrue ? (<Result data={data} />) : (null)}
            </div>
        </div>
    )
}

export default RollForm;