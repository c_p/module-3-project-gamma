from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import yelp_api
from routers import preferences
import os

app = FastAPI()

app.include_router(yelp_api.router)
app.include_router(preferences.router)

origins = [
    os.environ.get("CORS_HOST", "REACT_APP_AUTH_HOST"),
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
