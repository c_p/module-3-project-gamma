from fastapi.testclient import TestClient
from main import app
from routers.yelp_api import YelpApi

client = TestClient(app)

fake_yelp_data = {
    "term": "food",
    "price": "$",
    "location": "fremont",
    "radius": 10000,
    "limit": 15,
}


class ResponseObject:
    def json(self):
        return {"businesses": ["business1"]}


def test_yelp_details():
    class FakeYelpApi:
        async def call_yelp_api(self, headers, params):
            return ResponseObject()

    app.dependency_overrides[YelpApi] = FakeYelpApi

    response = client.get("/api/yelp-test?term=food&location=fremont")
    # print('response---------', response.json())
    assert response.json() == "business1"
    assert response.status_code == 200
