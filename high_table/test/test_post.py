from fastapi.testclient import TestClient
from queries.preferences import PreferenceQueries
from main import app
from routers.preferences import get_favorites
from authenticator import authenticator

client = TestClient(app)

fakeAcc = {"username": "username"}

fakePrefs = {
    "blacklist": "string",
}

fakePrefStatus = {"success": True}


async def account_out_override():
    return fakeAcc


app.dependency_overrides[
    authenticator.try_get_current_account_data
] = account_out_override


def test_preference_post():
    class fakePrefQuery:
        def add_to_blacklist(self, item, item2):
            pass

    app.dependency_overrides[PreferenceQueries] = fakePrefQuery

    response = client.post("/blacklist", json=fakePrefs)
    print(response.json())
    assert response.status_code == 200
    assert response.json() == fakePrefStatus
    app.dependency_overrides = {}
