from .client import Queries
from pydantic import BaseModel
from pymongo.errors import DuplicateKeyError
from typing import List
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class DuplicatePreferenceError(ValueError):
    pass


class FavoriteIn(BaseModel):
    favorite: str


class BlacklistIn(BaseModel):
    blacklist: str


class Blacklist(BaseModel):
    username: str
    blacklist: str
    _id: PydanticObjectId


class Blacklists(BaseModel):
    blacklists: List[Blacklist]


class Favorite(BaseModel):
    username: str
    favorite: str
    _id: PydanticObjectId


class Favorites(BaseModel):
    favorites: List[Favorite]


# Currently only save INDIVIDUAL strings into the DB,
# should it be appending to a list instead? blacklist: List[str] | None


class PreferenceQueries(Queries):
    DB_NAME = "mongo-data-account"
    COLLECTION = "preferences"

    def add_to_blacklist(self, blacklist: BlacklistIn, username: str) -> bool:
        props = blacklist.dict()
        props["username"] = username
        try:
            self.collection.insert_one(props)
        except DuplicateKeyError:
            raise DuplicatePreferenceError()

    def add_to_favorite(self, favorite: FavoriteIn, username: str) -> bool:
        props = favorite.dict()
        props["username"] = username
        try:
            self.collection.insert_one(props)
        except DuplicateKeyError:
            raise DuplicatePreferenceError()

    # Takes in username (needs to be a string) and the return has to be formatted
    # to validate Blacklists pydantic model.
    # Collects an aggregate from the DB, $match & $and inform that the return will be filtered,
    # and that these queries are to be used together (and instead of or.)
    # Username is getting pulled from routers/preferences line 78. $exists: True tells the func
    # to only pull collection rows where "blacklist" has a value.

    def get_user_blacklists(self, username: str) -> Blacklists:
        props = self.collection.aggregate(
            [
                {
                    "$match": {
                        "$and": [
                            {"username": username},
                            {"blacklist": {"$exists": True}},
                        ]
                    }
                }
            ]
        )
        if not props:
            print("Not a known username")
        blacklistPropsList = list(props)
        return Blacklists(blacklists=blacklistPropsList)

    def get_user_favorites(self, username: str) -> Favorites:
        props = self.collection.aggregate(
            [
                {
                    "$match": {
                        "$and": [
                            {"username": username},
                            {"favorite": {"$exists": True}},
                        ]
                    }
                }
            ]
        )
        if not props:
            print("Not a known username")
        favoritesPropsList = list(props)
        return Favorites(favorites=favoritesPropsList)
