import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from pydantic import BaseModel


class AccountRepo(BaseModel):
    pass


class Account(BaseModel):
    pass


class AccountOut(BaseModel):
    pass


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: AccountRepo,
    ):
        # Use your repo to get the account based on the
        # username (which could be an email)
        return accounts.get(username)

    def get_account_getter(
        self,
        # accounts: AccountRepo = Depends(),
        # accounts: Depends(self.get_current_account_data)
    ):
        # Return the accounts. That's it.
        return self.get_current_account_data()

    def get_hashed_password(self, account: Account):
        # Return the encrypted password value from your
        # account object
        return account.hashed_password

    def get_account_data_for_cookie(self, account: Account):
        # Return the username and the data for the cookie.
        # You must return TWO values from this method.
        return account.username, AccountOut(**account.dict())


authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
