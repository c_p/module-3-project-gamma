from fastapi import APIRouter, Depends
import requests
import os
import random

YELP_API_KEY = os.environ["YELP_API_KEY"]

router = APIRouter()


class YelpApi:
    async def call_yelp_api(self, headers, params):
        return requests.get(
            "https://api.yelp.com/v3/businesses/search", headers=headers, params=params
        )


@router.get("/api/yelp-test")
async def yelp_details(term, location, price=None, yelp_api: YelpApi = Depends()):
    headers = {"Authorization": "bearer %s" % YELP_API_KEY}
    params = {
        "term": term,
        "price": price,
        "location": location,
        "radius": 10000,
        "limit": 15,
    }
    yelp_response = await yelp_api.call_yelp_api(headers, params)
    yelp_json = yelp_response.json()
    try:
        choice = random_func(list(yelp_json["businesses"]))
        return choice
    except IndexError:
        return "Invalid Parameters"


def random_func(input):
    return random.choice(input)
