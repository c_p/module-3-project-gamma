from fastapi import Depends, HTTPException, status, Response, APIRouter, Request
from queries.preferences import (
    FavoriteIn,
    BlacklistIn,
    PreferenceQueries,
)
from pydantic import BaseModel
from authenticator import authenticator
from typing import Optional

router = APIRouter()


class PreferenceOut(BaseModel):
    success: bool


# I would probably need two back end endpoints, one to write to the collection with a
# list of favorites or blacklists for a specific user ID, and another to read favorities
# or blacklist restaurants based on a user ID

# Front end endpoint will get token for ID & post to backend endpoint w/ restaurant names
# Another front-end point would have to read and output restaurant names for that user ID


@router.post("/favorite", response_model=PreferenceOut)
async def add_favorite(
    favorite: FavoriteIn,
    preferences: PreferenceQueries = Depends(),
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
):
    preferences.add_to_favorite(favorite, account_data["username"])
    return PreferenceOut(success=True)


@router.post("/blacklist", response_model=PreferenceOut)
async def add_blacklist(
    blacklist: BlacklistIn,
    preferences: PreferenceQueries = Depends(),
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
):
    preferences.add_to_blacklist(blacklist, account_data["username"])
    return PreferenceOut(success=True)


@router.get("/getblacklist")
async def get_blacklists(
    preferences: PreferenceQueries = Depends(),
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
):
    return preferences.get_user_blacklists(account_data["username"])


@router.get("/getfavorite")
async def get_favorites(
    preferences: PreferenceQueries = Depends(),
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
):
    return preferences.get_user_favorites(account_data["username"])
